#-*- coding: utf-8 -*-
import time
import sys
import itertools
from optimized_python import *
from pure_python import *
if not hasattr(sys, 'pypy_version_info'):
    from cython_pure_python import cython_pure_python_bbs_function
    from cython_optimized_python import cython_optimized_python_bbs_function
    from cython_parallel import cython_parallel_bbs_function
from python_ctypes import python_ctypes_bbs_function
from settings import F, M, seed


def timeit(func):
    def wrap(*args, **kwargs):
        start_time = time.time()
        func(*args, **kwargs)
        end_time = time.time()
        return end_time - start_time
    return wrap


@timeit
def run_generator(gen, output_size):
    itertools.islice(gen, output_size - 1, output_size).next()


@timeit
def run_generator_parallel(generator):
    generator(F, M, seed, output_size)


def measure_performance(generator):
    print '--- Start BBS generator testing ---'
    gen = generator(M, seed)
    running_time = run_generator(gen, output_size)
    print 'Running time: %.3f seconds' % running_time
    print 'Performance: %i bits/sec' % (output_size / running_time)
    print '--- End BBS generator testing ---'


def measure_performance_parallel(generator, output_size):
    print '--- Start BBS generator testing ---'
    running_time = run_generator_parallel(generator)
    print 'Running time: %.3f seconds' % running_time
    print 'Performance: %i bits/sec' % (output_size / running_time)
    print '--- End BBS generator testing ---'


if __name__ == '__main__':
    # Get output size from cmd line
    # 'python main.py 10000000'
    if len(sys.argv) == 1:
        output_size = 1000000
    else:
        output_size = int(sys.argv[1])
    # Class-based generators
    measure_performance(StraightforwardPythonBBS)
    measure_performance(OptimizedPythonBBS)
    # Function-based generators
    measure_performance(straightforward_python_bbs_function)
    measure_performance(optimized_python_bbs_function)
    # Cython
    if not hasattr(sys, 'pypy_version_info'):
        measure_performance(cython_pure_python_bbs_function)
        measure_performance(cython_optimized_python_bbs_function)
        measure_performance_parallel(cython_parallel_bbs_function, output_size)
    # Ctypes
    measure_performance(python_ctypes_bbs_function)
