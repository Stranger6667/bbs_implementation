#-*- coding: utf-8 -*-
import random
import math

# Set primes
p = 42451
q = 46691
# Product of these primes.
M = 1982079641
F = 1981990500  # (p - 1) * (q - 1)
# Initial condition. It is doesn't matter now, for prototyping. In real it will
# be taken from hardware sources (and will be truly random). Now it chosen to
# satisfies following condition: "seed ** 2 > M". This excludes stuck on first
# iteration.
seed = random.randint(int(math.sqrt(M)) + 1, M)
