# coding=utf-8
from cython.parallel import prange
cimport cython

@cython.cdivision(True)
@cython.boundscheck(False)
def cython_parallel_bbs_function(unsigned long long F, unsigned long long M,
                                 unsigned long long seed,
                                 unsigned long long output_size):
    cdef unsigned char value
    cdef unsigned long long i
    cdef unsigned long long two_pow
    cdef unsigned long long seed_pow

    for i in prange(output_size, nogil=True, schedule='dynamic'):
        two_pow = 2 ** i
        seed_pow = two_pow % F
        value = seed ** seed_pow % M
