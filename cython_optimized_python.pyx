#-*- coding: utf-8 -*-


def cython_optimized_python_bbs_function(unsigned long long M,
                                         unsigned long long seed):
    cdef unsigned char value
    while True:
        value = seed & 1
        yield value
        seed = (seed * seed) % M
