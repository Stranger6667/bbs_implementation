# coding=utf-8
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy


# I use GCC-4.2 x86_64 on macosx-10.6
# >>> cython cython_pure_python.pyx
# >>> cython cython_optimized_python.pyx
# >>> cython cython_parallel.pyx
# >>> python setup.py build_ext --inplace
setup(
    cmdclass={'build_ext': build_ext},
    ext_modules=[
        Extension("cython_pure_python", ["cython_pure_python.pyx"]),
        Extension("cython_optimized_python", ["cython_optimized_python.pyx"]),
        Extension("cython_parallel", ["cython_parallel.pyx"],
                  include_dirs=[numpy.get_include()],
                  extra_compile_args=['-fopenmp'],
                  extra_link_args=['-fopenmp'],
        )
    ],
    requires=['Cython', 'numpy']
)
