#-*- coding: utf-8 -*-
from ctypes import *


def python_ctypes_bbs_function(M, seed):
    libbbs = CDLL('libbbs.so')
    libbbs.bbs.argtypes = [c_ulonglong, c_ulonglong]
    libbbs.bbs.restype = c_ulonglong
    while True:
        yield seed & 1
        seed = libbbs.bbs(M, seed)
