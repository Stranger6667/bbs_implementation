#-*- coding: utf-8 -*-


def cython_pure_python_bbs_function(M, seed):
    while True:
        yield seed & 1
        seed = (seed * seed) % M
