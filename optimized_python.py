#-*- coding: utf-8 -*-


class OptimizedPythonBBS(object):
    def __init__(self, M, seed):
        self.M = M
        self.seed = seed
        self.current_state = seed

    def __iter__(self):
        return self

    def next(self):
        current_state = self.current_state
        current_state = (current_state * current_state) % self.M
        self.current_state = current_state
        return current_state & 1


def optimized_python_bbs_function(M, seed):
    while True:
        yield seed & 1
        seed = (seed * seed) % M
