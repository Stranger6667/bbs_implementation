#-*- coding: utf-8 -*-


class StraightforwardPythonBBS(object):
    def __init__(self, M, seed):
        self.M = M
        self.seed = seed
        self.current_state = seed

    def __iter__(self):
        return self

    def next(self):
        self.current_state = (self.current_state ** 2) % self.M
        return self.lowest_bit

    @property
    def lowest_bit(self):
        return bin(self.current_state)[-1]


def straightforward_python_bbs_function(M, seed):
    while True:
        yield bin(seed)[-1]
        seed = (seed ** 2) % M
